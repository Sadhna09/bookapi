﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Models
{
    public class BookContext : DbContext
    {
        public BookContext(DbContextOptions<BookContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    Id = 1,
                    Title = "Casper",
                    Author = "Cia",
                    Description = "Fiction"

                },
                new Book
                {
                    Id = 2,
                    Title = "Cipher",
                    Author = "sanu",
                    Description = "Fiction"
                });

        }
        public DbSet<Book> Books { get; set; }
    }
}
